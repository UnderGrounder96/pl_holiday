import "dotenv/config";
import { defineConfig } from "vite";
import preact from "@preact/preset-vite";

// https://vitejs.dev/config/
export default defineConfig({
  envPrefix: "APP_",
  css: {
    devSourcemap: true,
  },
  server: {
    host: true,
    strictPort: true,
    port: process.env.APP_DEV_PORT || 3000,
  },
  preview: {
    port: process.env.APP_PROD_PORT || 9000,
  },
  plugins: [preact()],
});

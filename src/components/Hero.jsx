import { useEffect, useState } from "preact/hooks";

import { calculateEaster } from "./basic/calculateEaster.jsx";
import { data } from "./basic/data.jsx";

const HolidayList = ({ curYear }) => {
  const dateConverter = (holidayDate, stale, text, tooltip) => {
    const date = new Date();
    const dayNames = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    if (stale) {
      const [day, month] = holidayDate.split("-");
      date.setFullYear(curYear, month - 1, day);
    } else {
      const splittedText = text.split(" ");
      const [Y, M, D] = calculateEaster(curYear);
      date.setFullYear(Y, M - 1, D);

      switch (splittedText[0]) {
        case "easter":
          if (splittedText[1] == "monday") {
            date.setDate(date.getDate() + 1);
          }
          break;

        case "pentecost":
          date.setDate(date.getDate() + 49);
          break;

        case "corpus":
          date.setDate(date.getDate() + 60);
          break;
      }
    }

    return (
      <>
        <td>{`${date.getDate()} ${monthNames[date.getMonth()]}`}</td>
        <td>{dayNames[date.getDay()]}</td>
        <td class="hover-text">
          {text} <span class="tooltip-text">{tooltip}</span>
        </td>
      </>
    );
  };

  return (
    <table class="holiday-table">
      <tr class="table-content table-head">
        <th>date</th>
        <th>day</th>
        <th>holiday</th>
      </tr>

      {data.map(({ id, date, stale, text, tooltip }) => (
        <tr key={id} class="table-content table-body">
          {dateConverter(date, stale, text, tooltip)}
        </tr>
      ))}
    </table>
  );
};

const SideBar = ({ curYear, setCurYear }) => {
  const [extraLi, setExtraLi] = useState(null);
  const title = <h3>Years</h3>;
  const years = [-3, -2, -1, 1, 2, 3];

  useEffect(() => {
    const checkCurYear = () => {
      const realCurYear = new Date().getFullYear();

      for (const item of years) {
        if (curYear === realCurYear || curYear + item === realCurYear) {
          setExtraLi(null);
          return;
        }
      }

      setExtraLi(
        <li key={realCurYear}>
          <a
            href="#"
            onclick={() => {
              setCurYear(realCurYear);
            }}
          >
            {realCurYear} [present]
          </a>
        </li>,
      );
    };

    checkCurYear();
  });

  return (
    <aside>
      {title}
      <ul>
        {extraLi}
        {years.map((item) => (
          <li key={curYear + item}>
            <a
              href="#"
              onclick={() => {
                setCurYear(curYear + item);
              }}
            >
              {curYear + item}
            </a>
          </li>
        ))}
      </ul>
    </aside>
  );
};

export default function Hero() {
  const [curYear, setCurYear] = useState(new Date().getFullYear());
  const title = <h1 class="title">holiday planning ({curYear})</h1>;

  return (
    <main class="section-center">
      {title}

      <div class="main-container">
        <HolidayList curYear={curYear} setCurYear={setCurYear} />

        <SideBar curYear={curYear} setCurYear={setCurYear} />
      </div>
    </main>
  );
}

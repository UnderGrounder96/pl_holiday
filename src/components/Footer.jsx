import { Socials } from "./Socials.jsx";

const Copyright = () => {
  const date = new Date().getFullYear();

  return (
    <h4 class="footer-text text-small">
      &copy; <span id="date">{date}</span>
      <span class="company">company</span>. all rights reserved
    </h4>
  );
};

export default function Footer() {
  return (
    <footer class="section-center">
      <Socials />

      <Copyright />
    </footer>
  );
}

import { links } from "./basic/links.jsx";

export const Socials = () => {
  return (
    <div class="social-icons">
      {links.map(({ id, social, link }) => (
        <a
          key={id}
          href={link}
          target="_blank"
          rel="noreferrer"
          class="social-icon"
        >
          <i class={`fa-brands fa-${social}`}></i>
        </a>
      ))}
    </div>
  );
};

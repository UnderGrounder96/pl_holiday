export const links = [
  {
    id: 1,
    social: "tiktok",
    link: "https://tiktok.com/@undergrounder96",
  },
  {
    id: 2,
    social: "twitter",
    link: "https://twitter.com/undergrounder96",
  },
  {
    id: 3,
    social: "instagram",
    link: "https://instagram.com/undergrounder96",
  },
];

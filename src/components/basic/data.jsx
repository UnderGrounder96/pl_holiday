export const data = [
  {
    id: 1,
    date: "01-01",
    text: "new year",
    stale: true,
    tooltip:
      "New Year is celebrated with festivities, in comemoration the end of previous year",
  },
  {
    id: 2,
    date: "06-01",
    stale: true,
    text: "epiphany",
    tooltip:
      "Epiphany day is a religious holiday, celebrated 12 days after Christmas",
  },
  {
    id: 3,
    date: "31-03",
    stale: false,
    text: "easter sunday",
    tooltip: "Easter is a religous celebration of Jesus' resurrection",
  },
  {
    id: 4,
    date: "01-04",
    stale: false,
    text: "easter monday",
    tooltip: "The Monday right after Easter",
  },
  {
    id: 5,
    date: "01-05",
    stale: true,
    text: "labour",
    tooltip: "International Labour day, in celebration of all workers",
  },
  {
    id: 6,
    date: "03-05",
    stale: true,
    text: "constitution",
    tooltip:
      "Commemoration of Poland's constitution day, established democratically in 1791",
  },
  {
    id: 7,
    date: "19-05",
    stale: false,
    text: "pentecost",
    tooltip:
      "Pentecost is a Christian holiday which takes place on the 50th day after Easter Sunday",
  },
  {
    id: 8,
    date: "30-05",
    stale: false,
    text: "corpus christi",
    tooltip:
      "Corpus Christi is a Roman Catholic holy day that has been observed since the 13th Century A.D. to celebrate the Holy Eucharist",
  },
  {
    id: 9,
    date: "15-08",
    stale: true,
    text: "assumption",
    tooltip:
      "Assumption day is a public holiday observed every 15th Aug. Poland's Armed Forces Day is also observed on same date",
  },
  {
    id: 10,
    date: "01-11",
    stale: true,
    text: "all saints",
    tooltip:
      "Also known as 'Day of the Dead', is an international public holiday observed at different dates depending on the country",
  },
  {
    id: 11,
    date: "11-11",
    stale: true,
    text: "independence",
    tooltip:
      "Poland's Independence Day is observed every November 11th, coincidently same date as Angola",
  },
  {
    id: 12,
    date: "25-12",
    stale: true,
    text: "christmas",
    tooltip: `Christmas in Poland is known as "Boze Narodzenie" ("God's Birth") after the birth of Christ, the Divine Savior`,
  },
  {
    id: 13,
    date: "26-12",
    stale: true,
    text: "2nd day christmas",
    tooltip: "The day after Christmas, is also considered as public holiday",
  },
];

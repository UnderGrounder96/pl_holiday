import { Socials } from "./Socials.jsx";

import imgLogo from "../assets/pl_holiday_logo.png";

const Logo = () => {
  return (
    <a href="/">
      <img class="nav-logo" src={imgLogo} alt="logo" />
    </a>
  );
};

export default function Header() {
  return (
    <header>
      <nav class="section-center">
        <Logo />

        <Socials />
      </nav>
    </header>
  );
}

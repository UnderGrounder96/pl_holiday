import { render } from "preact";
import { LocationProvider, Route, Router } from "preact-iso";

import { Home } from "./pages/Home.jsx";
import { NotFound } from "./pages/404.jsx";

import Header from "./components/Header.jsx";
import Footer from "./components/Footer.jsx";

import "./styles/main.scss";

export function App() {
  return (
    <LocationProvider>
      <Header />
      <section class="main">
        <Router>
          <Route path="/" component={Home} />
          <Route default component={NotFound} />
        </Router>
      </section>
      <Footer />
    </LocationProvider>
  );
}

render(<App />, document.getElementById("app"));
